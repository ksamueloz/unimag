$(function () {
    //LLamada a la función Tolltip y Popover
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    //Tiempo de 2 segundos para el carousel.
    $('.carousel').carousel({
        interval: 2000
    });
    //Modal con Jquery Idioma
    $('#idiomas').on('show.bs.modal', function (e) {
        console.log('Modal idioma se está mostrando');
        //Botón de pago
        $('#idiomasBtn').removeClass('btn-outline-success');
        $('#idiomasBtn').addClass('btn-primary');
    });
    $('#idiomas').on('shown.bs.modal', function (e) {
        console.log('Modal idioma se mostró');
    });
    $('#idiomas').on('hide.bs.modal', function (e) {
        console.log('Modal idioma se oculta');
        $('#idiomasBtn').removeClass('btn-primary');
        $('#idiomasBtn').addClass('btn-outline-success');
    });
    $('#idiomas').on('hidden.bs.modal', function (e) {
        console.log('Modal idioma se ocultó');
    });

    //Modal con Jquery Cursos
    $('#cursos').on('show.bs.modal', function (e) {
        console.log('Modal cursos se está mostrando');
        //Botón de pago
        $('#cursosBtn').removeClass('btn-outline-success');
        $('#cursosBtn').addClass('btn-primary');
    });
    $('#cursos').on('shown.bs.modal', function (e) {
        console.log('Modal cursos se mostró');
    });
    $('#cursos').on('hide.bs.modal', function (e) {
        console.log('Modal cursos se oculta');
        $('#cursosBtn').removeClass('btn-primary');
        $('#cursosBtn').addClass('btn-outline-success');
    });
    $('#cursos').on('hidden.bs.modal', function (e) {
        console.log('Modal cursos se ocultó');
    });
    //Modal con Jquery Pagos
    $('#pagos').on('show.bs.modal', function (e) {
        console.log('Modal pago se está mostrando');
        //Botón de pago
        $('#pagosBtn').removeClass('btn-outline-success');
        $('#pagosBtn').addClass('btn-primary');
    });
    $('#pagos').on('shown.bs.modal', function (e) {
        console.log('Modal pago se mostró');
    });
    $('#pagos').on('hide.bs.modal', function (e) {
        console.log('Modal pago se oculta');
        $('#pagosBtn').removeClass('btn-primary');
        $('#pagosBtn').addClass('btn-outline-success');
    });
    $('#pagos').on('hidden.bs.modal', function (e) {
        console.log('Modal pago se ocultó');
    });
});